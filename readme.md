Drop BLAST
==========

Drops all but the first two columns of a TSV file.
Used for obtaining just the connectivity information from BLAST.


Usage
-----

`dropblast`

* `STDIN` - input BLAST
* `STDOUT` - output BLAST


Example
-------

`dropblast < plasmids.blast > plasmids.txt`


Meta
----

```ini
type=command-line, application
tags=blast, tsv
language=python3
```