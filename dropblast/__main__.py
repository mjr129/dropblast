"""
See readme.md for details.
"""
import sys


for line in sys.stdin:
    line = line.split("\t", 2)
    sys.stdout.write(line[0])
    sys.stdout.write("\t")
    sys.stdout.write(line[1])
    sys.stdout.write("\n")
